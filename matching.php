<?php
include_once('config.php');
if(!isset($_SESSION)){
 session_start();
}
IF (!isset($_SESSION['username']))
  	{ header('Location: '. $homepage); }

$con = openConnection();
$_SESSION['background'] = mysql_result ( mysql_query ("SELECT Background FROM user WHERE Username = '".$_SESSION['username']."' "), 0);

$_SESSION['query'] = ""; 
// controle voor invoer. If oke -> header result.php else errors maken
if (isset($_POST['controle']) AND $_POST['controle'] == "TRUE") {

	if(!isset($_POST['match_all'])){
		if(
				(!isset($_POST['gender']))
			OR	(!isset($_POST['prefsex']))
			OR	($_POST['province'] == "Province" AND !isset($_POST['province_all']))
			OR	($_POST['hair'] == "Hair Color" AND !isset($_POST['hair_all']))
			OR 	($_POST['eye'] == "Eye Color" AND !isset($_POST['eye_all']))
			OR 	($_POST['skin'] == "Skin Color" AND !isset($_POST['skin_all']))
			OR 	($_POST['religion'] == "Religion" AND !isset($_POST['religion_all']))
			OR 	($_POST['education'] == "Education" AND !isset($_POST['education_all']))
			OR 	($_POST['children'] == "Children" AND !isset($_POST['children_all']))
			OR 	($_POST['childwish'] == "Childwish" AND !isset($_POST['childwish_all']))
			OR 	(!isset($_POST['smoke']))
			OR 	( 	( ($_POST['agemin'] == "Age") OR ($_POST['agemax'] == "Age") ) AND ( $_POST['age'] != "<>" ) )
		) { 
			$error=1; $error_head="<b>!! ERROR !!</b>"; 
		}
	}

	if ($error) {
		if (!isset($_POST['gender'])) 												{ $error_gender = "You need to choose which <u>Gender</u> you want to sort on!"; }
		if (!isset($_POST['prefsex']))												{ $error_prefsex = "You need to choose which <u>Preferred gender</u> you want to sort on!"; }
		if ($_POST['province'] == "Province" AND !isset($_POST['province_all']))	{ $error_province = "You need to choose which <u>Province</u> you want to sort on!"; }
		if ($_POST['hair'] == "Hair Color" AND !isset($_POST['hair_all']))			{ $error_hair = "You need to choose which <u>Hair Color</u> you want to sort on!"; }
		if ($_POST['eye'] == "Eye Color" AND !isset($_POST['eye_all'])) 			{ $error_eye = "You need to choose which <u>Eye Color</u> you want to sort on!"; }
		if ($_POST['skin'] == "Skin Color" AND !isset($_POST['skin_all']))	 		{ $error_skin = "You need to choose which <u>Skin Color</u> you want to sort on!"; }
		if ($_POST['religion'] == "Religion" AND !isset($_POST['religion_all']))	{ $error_religion = "You need to choose which <u>Religion</u> you want to sort on!"; }
		if ($_POST['education'] == "Education" AND !isset($_POST['education_all']))	{ $error_education = "You need to choose which <u>Education</u> you want to sort on!"; }
		if ($_POST['children'] == -1 AND !isset($_POST['children_all'])) 			{ $error_children = "You need to choose how many <u>Children</u> you want to sort on!"; }
		if ($_POST['childwish'] == -1 AND !isset($_POST['childwish_all']))			{ $error_childwish = "You need to choose which <u>Childwish</u> you want to sort on!"; }
		if (!isset($_POST['smoke']) )												{ $error_smoke = "You need to choose whether <u>Smoking</u> is allowed!"; }
		if (($_POST['agemin'] == "Age") && (!isset($_POST['age']) ))				{ $error_age = "You need to choose which <u>Age</u> you want to sort on!"; }
		if (($_POST['agemax'] == "Age") && (!isset($_POST['age']) ))				{ $error_age = "You need to choose which <u>Age</u> you want to sort on!"; }

	} else {

		$sql_match=("SELECT * FROM user WHERE Username <> '".$_SESSION['username']."' ");
		if (!isset($_POST['match_all'])){
		if ($_POST['gender'] != "<>")	{ $sql_match .= "AND Geslacht = '".$_POST['gender']."' "; }
		if ($_POST['prefsex'] != "<>")	{ $sql_match .= "AND PrefSex = '".$_POST['prefsex']."' "; }
		if ($_POST['province'] != "<>") { $sql_match .= "AND Provincie = '".$_POST['province']."' "; }
		if ($_POST['hair'] != "<>") 	{ $sql_match .= "AND Haarkleur = '".$_POST['hair']."' "; }
		if ($_POST['eye'] != "<>") 		{ $sql_match .= "AND Oogkleur = '".$_POST['eye']."' "; }
		if ($_POST['skin'] != "<>") 	{ $sql_match .= "AND Huidskleur = '".$_POST['skin']."' "; }
		if ($_POST['religion'] != "<>") { $sql_match .= "AND Religie = '".$_POST['religion']."' "; }
		if ($_POST['education'] != "<>"){ $sql_match .= "AND Opleiding = '".$_POST['education']."' "; }
		if ($_POST['children'] != "<>") { $sql_match .= "AND Kinderen ".$_POST['chorder'] . " '" . $_POST['children']."' "; }
		if ($_POST['childwish'] != "<>"){ $sql_match .= "AND Kinderwens ".$_POST['cworder'] . " '" . $_POST['childwish']."' "; }
		if ($_POST['smoke'] != "<>") 	{ $sql_match .= "AND Roken = '".$_POST['smoke']."' "; }
		if ($_POST['age'] != "<>") 		{ $sql_match .= "AND Jaar <= ".( date("Y") - $_POST['agemin'] )." AND (
			(Maand = ".date("m")." AND Dag <= ".date("d").") OR (Maand < ".date("m")." ) )
			AND ( ( Jaar >= ". (date("Y") - $_POST['agemax']) .") OR 
			( Jaar >= ". (date("Y") - $_POST['agemax'] - 1) ." AND ( 
			( Maand > ". date("m") ." ) OR ( Maand = ".date("m")." AND Dag > ".date("d") ." )
			) ) ) " ; }
		}

		$_SESSION['query'] = $sql_match;

		header("Location: result.php");
	} 
}
?>

<html>
<head>
<title>Matching - <?php echo $_SESSION['username']; ?> </title>
<link rel="icon" type="image/ico" href="favicon.ico"> </link>
</head>
<body bgcolor="<?php echo $background; ?>">

<?php include('menu.php'); ?>
<hr>
<center><big><big><b>Matching</b></big></big></center>
<b>On which attributes do you wish to select?</b>

<br><font color="#FF0000"><?php if(isset($error_head)){echo $error_head;} ?></font> </br>
<form action="" method="post">
<input type="hidden" name="controle" value="TRUE">

<table><tr> <td width=120> Gender: </td>
<td width=100><input type="checkbox" name="gender" value="<>" <?php if(isset($_POST['gender']) AND $_POST['gender'] == "<>" ) { echo 'checked'; } ?>>I don't mind</td>
<td width=310>
<input type="radio" name="gender" value="Male" <?php if(isset($_POST['gender']) AND $_POST['gender'] == "Male" ) { echo 'checked'; } ?>>Male 
<input type="radio" name="gender" value="Female" <?php if(isset($_POST['gender']) AND $_POST['gender'] == "Female" ) { echo 'checked'; } ?>>Female
</td> <td> <font color="#FF0000"><?php if(isset($error_gender)){echo $error_gender;} ?></font></td></tr>

<tr><td>Preferred Gender: </td>
<td><input type="checkbox" name="prefsex" value="<>" <?php if(isset($_POST['prefsex']) AND $_POST['prefsex'] == "<>") { echo 'checked'; } ?>>I don't mind</td>
<td><input type="radio" name="prefsex" value="Male" <?php if(isset($_POST['prefsex']) AND $_POST['prefsex'] == "Male") { echo 'checked'; } ?>>Male
<input type="radio" name="prefsex" value="Female" <?php if(isset($_POST['prefsex']) AND $_POST['prefsex'] == "Female") { echo 'checked'; } ?>>Female
</td> <td><font color="#FF0000"><?php if(isset($error_prefsex)){echo $error_prefsex;} ?></font> </td></tr>

<tr><td>Province: </td>
<td> <input type="checkbox" name="province_all" value="<>" <?php if(isset($_POST['province_all'])) { echo 'checked'; } ?>>I don't mind </td>
<td><?php
$province = array("Province", "Drenthe", "Flevoland", "Friesland", "Gelderland", "Groningen", "Limburg", 
	"Noord-Braband", "Noord-Holland", "Overijssel", "Utrecht", "Zeeland", "Zuid-Holland");
echo '<select name="province">';
for($p=0; $p<13; $p++){
   if($province[$p] == $_POST["province"] ){$selected = 'selected="selected"';}else{$selected = '';}
   echo '<option value="'.$province[$p].'" '.$selected.'>'.$province[$p].'</option>'; }
echo '</select>';
?></td><td> <font color="#FF0000"><?php if(isset($error_province)){echo $error_province;} ?> </td></tr>

<tr><td>Hair Color: </td>
<td> <input type="checkbox" name="hair_all" value="<>" <?php if(isset($_POST['hair_all'])){ echo 'checked'; } ?>>I don't mind </td>
<td><?php
$hair= array("Hair Color", "White", "Light Blonde", "Blonde", "Light Brown", "Brown", "Grey", "Black", "Light Red", "Red", "Other");
echo '<select name="hair">';
for($h=0; $h<11; $h++){
	if($hair[$h] == $_POST["hair"]) {$selected='selected="selected"'; } else {$selected='';}
	echo '<option value="'.$hair[$h].'" '.$selected. '>'.$hair[$h].'</option>'; }
echo '</select>';
?> </td> <td><font color="#FF0000"><?php if(isset($error_hair)){echo $error_hair;} ?> </td></tr>

<tr><td>Eye Color: </td>
<td><input type="checkbox" name="eye_all" value="<>" <?php if(isset($_POST['eye_all'])) { echo 'checked'; } ?>>I don't mind </td>
<td> <?php
$eye = array("Eye Color", "Blue", "Blue-Green", "Green", "Brown");
echo '<select name="eye">';
for($e=0; $e<5; $e++){
	if($eye[$e] == $_POST["eye"]) {$selected='selected="selected"'; } else {$selected='';}
	echo '<option value="'.$eye[$e].'" '.$selected. '>'.$eye[$e].'</option>'; }
echo '</select>';
?> <td><font color="#FF0000"><?php if(isset($error_eye)){echo $error_eye;} ?> </td></tr>

<tr><td>Skin Color: </td>
<td> <input type="checkbox" name="skin_all" value="<>" <?php if(isset($_POST['skin_all'])) { echo 'checked'; } ?>>I don't mind </td>
<td> <?php
$skin = array("Skin Color", "White", "Lightly Tinted", "Medium Tinted", "Heavily Tinted", "Brown", "Black");
echo '<select name="skin">';
for($s=0; $s<7; $s++){
	if($skin[$s] == $_POST["skin"]) {$selected='selected="selected"'; } else {$selected='';}
	echo '<option value="'.$skin[$s].'" '.$selected. '>'.$skin[$s].'</option>'; }
echo '</select>';
?> <td><font color="#FF0000"><?php if(isset($error_skin)){echo $error_skin;} ?> </td></tr>

<tr><td>Religion: </td>
<td><input type="checkbox" name="religion_all" value="<>" <?php if(isset($_POST['religion_all'])) { echo 'checked'; } ?>>I don't mind </td>
<td> <?php
$religion = array("Religion", "Cristian", "Catholic", "Reformed", "Protestant", "Moslim", "Boeddhism", "Hindoeism", "Jewish", "Other", "Atheist");
echo '<select name="religion">';
for($r=0; $r<11; $r++){
	if($religion[$r] == $_POST["religion"]) {$selected='selected="selected"'; } else {$selected='';}
	echo '<option value="'.$religion[$r].'" '.$selected. '>'.$religion[$r].'</option>'; }
echo '</select>';
?> </td> <td><font color="#FF0000"><?php if(isset($error_religion)){echo $error_religion;} ?> </td></tr>

<tr><td>Education: </td>
<td><input type="checkbox" name="education_all" value="<>" <?php if(isset($_POST['education_all'])) { echo 'checked'; } ?>>I don't mind </td>
<td> <?php
$education = array("Education", "VMBO", "HAVO", "VWO", "Grammar School", "Vocational", "College", "University");
echo '<select name="education">';
for($ed=0; $ed<8; $ed++){
	if($education[$ed] == $_POST["education"]) {$selected='selected="selected"'; } else {$selected='';}
	echo '<option value="'.$education[$ed].'" '.$selected. '>'.$education[$ed].'</option>'; }
echo '</select>';
?></td> <td><font color="#FF0000"><?php if(isset($error_education)){echo $error_education;} ?> </td></tr>

<tr><td>Children: </td>
<td><input type="checkbox" name="children_all" value="<>" <?php if( isset($_POST['children_all'])) { echo 'checked'; } ?>>I don't mind </td>
<td> <?php
$order = array("<=", "At most:", "=", "Exactly:", ">=", "At least");
$children= array("Children", "0", "1", "2", "3", "4", "5 or more");

echo '<select name="chorder">';
for ($cho=0; $cho<6; $cho+=2) {
	if ($order[$cho] == $_POST["chorder"]) {$selected='selected="selected"'; } else {$selected=''; }
	echo '<option value="'. $order[$cho] . '" '. $selected . '>' . $order[$cho+1].'</option>'; }
echo '</select>';

echo '<select name="children">';
for($ch=0; $ch<7; $ch++){
	if($children[$ch] == $_POST["children"]) {$selected='selected="selected"'; } else {$selected='';}
	echo '<option value="'. ($ch-1) .'" '.$selected. '>'.$children[$ch].'</option>'; }
echo '</select>';
?></td> <td><font color="#FF0000"><?php if(isset($error_children)){echo $error_children;} ?> </td></tr>

<tr><td>Childwish: </td>
<td><input type="checkbox" name="childwish_all" value="<>" <?php if(isset($_POST['childwish_all'])) { echo 'checked'; } ?>>I don't mind </td>
<td> <?php
$childwish= array("Childwish", "0", "1", "2", "3", "4", "5 or more");

echo '<select name="cworder">';
for ($cwo=0; $cwo<6; $cwo+=2) {
	if ($order[$cwo] == $_POST["cworder"]) {$selected='selected="selected"'; } else {$selected=''; }
	echo '<option value="'. $order[$cwo] . '" '.$selected. '>' . $order[$cwo+1].'</option>'; }
echo '</select>';


echo '<select name="childwish">';
for($cw=0; $cw<7; $cw++){
	if($childwish[$cw] == $_POST["childwish"]) {$selected='selected="selected"'; } else {$selected='';}
	echo '<option value="'. ($cw-1) .'" '.$selected. '>'.$childwish[$cw].'</option>'; }
echo '</select>';
?> </td> <td><font color="#FF0000"><?php if(isset($error_childwish)){echo $error_childwish;} ?> </td></tr>

<tr><td>Smoking: </td>
<td><input type="checkbox" name="smoke" value="<>" <?php if(isset($_POST['smoke']) AND $_POST['smoke'] == "<>" ) { echo 'checked'; } ?>>I don't mind</td>
<td><input type="radio" name="smoke" value="yes" <?php if(isset($_POST['smoke']) AND $_POST['smoke'] == "yes" ) { echo 'checked'; } ?>>Allowed to smoke
<input type="radio" name="smoke" value="no" <?php if(isset($_POST['smoke']) AND $_POST['smoke'] == "no" ) { echo 'checked'; } ?>><b>Not</b> allowed to smoke
</td> 
<td><font color="#FF0000"> <?php if(isset($error_smoke)){echo $error_smoke;} ?> </td></tr>

<tr><td>Ages: </td>
<td><input type="checkbox" name="age" value="<>" <?php if(isset($_POST['age']) AND $_POST['age'] == "<>" ) { echo 'checked'; } ?> >I don't mind </td>
<td>Between <?php
$agemin = array("Age", "18", "25", "30", "35", "40", "45", "50", "55", "60", "65", "70", "75");
echo '<select name="agemin">';
for($a1=0; $a1<13; $a1++){
	if($agemin[$a1] == $_POST["agemin"]) {$selected='selected="selected"'; } else {$selected='';}
	echo '<option value="'.$agemin[$a1].'" '.$selected. '>'.$agemin[$a1].'</option>'; }
echo '</select>'; ?>
and
<?php
$agemax = array("Age", "25", "30", "35", "40", "45", "50", "55", "60", "65", "70", "75", "80");
echo '<select name="agemax">';
for($a2=0; $a2<13; $a2++){
	if($agemax[$a2] == $_POST["agemax"]) {$selected='selected="selected"'; } else {$selected='';}
	echo '<option value="'.$agemax[$a2].'" '.$selected. '>'.$agemax[$a2].'</option>'; }
echo '</select>'; ?>
years old. </td>
<td><font color="#FF0000"><?php if(isset($error_age)){echo $error_age;} ?> </td></tr> 
</table><br>

<input type="checkbox" name="match_all" value="TRUE">Match me to everyone!
<input type="submit" value="Match!"></form> 

<form name="reset" method="post" action="<?php echo ($_SERVER["PHP_SELF"]);?>">
<input type="submit" value="Reset">
</form>
<hr>
<form action="profile.php?u=<?php echo $_SESSION['username']; ?>" method="post">
<input type="submit" value="Go to my own profile!"></form>
</body>
</html>