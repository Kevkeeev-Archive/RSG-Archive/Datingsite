<?php
$lang = array();
$lang['match'] = "Matching";
$lang['found1'] = "We found";
$lang['found2'] = "result";
$lang['found3'] = ":";
$lang['found4'] = "s:";
$lang['found5'] = "";
$lang['nomatch'] = "Do you want to match again? <br><br>";
$lang['name'] = "Name";
$lang['age'] = "Age";
$lang['gender'] = "Gender";
$lang['prefer'] = "Preferred";
$lang['province'] = "Province";
$lang['hair'] = "Hair colour";
$lang['skin'] = "Skin colour";
$lang['eye'] = "Eye colour";
$lang['religion'] = "Religion";
$lang['school'] = "Education";
$lang['kids'] = "Children";
$lang['wish'] = "Childwish";
$lang['smoke'] = "Smoking";
$lang['sort'] = "Sort on:";
$lang['back'] = "Back to matching screen";
$lang['profile'] = "Go to my profile";

$sort = array("Age", "Gender", "Preferred", "Province", "Hair Colour", "Eye Colour", 
	"Skin Colour", "Religion", "Education", "Children", "Childwish", "Smoking");
$wayer=array("Ascending", "Descending");

?>