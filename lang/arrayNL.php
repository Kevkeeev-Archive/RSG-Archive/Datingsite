<?php
$gender = array("Geslacht", "Man", "Vrouw");
$prefer = array("Voorkeur", "Man", "Vrouw", "Beide");
$province = array("Provincie", "Drenthe", "Flevoland", "Friesland", "Gelderland", "Groningen", "Limburg", "Noord-Brabant", "Noord-Holland", "Overijssel", "Utrecht", "Zeeland", "Zuid-Holland");
$religion = array("Religie", "Christelijk", "Katholiek", "Gereformeerd", "Protestant", "Moslim", "Boeddhisme", "Hindoeisme", "Joods", "Anders...", "Atheist");
$school = array("Opleiding", "VMBO", "HAVO", "VWO", "Gymnasium", "MBO", "HBO", "Universitair");
$skin = array("Huidskleur", "Blank", "Licht Getint", "Middel Getint", "Zwaar Getint", "Bruin", "Zwart");
$hair = array("Haarkleur", "Wit", "Licht Blond", "Blond", "Licht Bruin", "Bruin", "Grijs", "Zwart", "Licht Rood", "Rood", "Anders...");
$eye = array("Oogkleur", "Blauw", "Blauw Groen", "Groen", "Bruin");
$smoke = array("Roken", "Ik rook wel", "Ik rook niet");
$kids = array("Kinderen",  "0", "1", "2", "3", "4", "5 of meer");
$wish = array("Kinderwens", "0", "1", "2", "3", "4", "5 of meer");

$maanden = array("Januari", "Februari", "Maart", "April", "Mei", "Juni", 
	"Juli", "Augustus", "September", "Oktober", "November", "December");
?>