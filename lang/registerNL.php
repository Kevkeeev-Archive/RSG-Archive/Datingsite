<?php
$error_user_empty="Je hebt je <u>Gebruikersnaam</u> niet ingevuld!<br>";
$error_user_taken="Die <u>Gebruikersnaam</u> is al bezet!<br>";
$error_pass_diff="Je hebt niet twee keer <u>hetzelfde</u> wachtwoord ingevoerd!<br>";
$error_pass_empty="Je moet je wachtwoord <u>twee keer</u> invullen!<br>";
$error_pass_empty_both="Je moet <u>beide wachtwoorden</u> invullen!<br>";
$error_email_empty="Je hebt je <u>e-mail adres</u> niet ingevuld!<br>";
$error_leeftijd_empty="Je hebt je <u>Geboortedatum</u> niet ingevuld!<br>";
$error_leeftijd_tejong = "Je bent niet <u>oud genoeg</u> om te registreren!<br>";

$lang = array();
$lang["registration"] = "Registreren";
$lang["register"] = "REGISTREER";
$lang["username"] = "Wat is je gebruikersnaam?";
$lang["password"] = "Wat is je wachtwoord?";
$lang["controle"] = "Wachtwoord voor controle:";
$lang["email"] = "Wat is je e-mail adres?";
$lang["geboorte"] = "Wat is je geboortedatum?";
$lang["leeftijd"]  = "BELANGRIJK! Je moet minstens 18 jaar zijn!";
$lang["submit"] = "Verstuur";
$lang["reset"] = "Reset";

?>