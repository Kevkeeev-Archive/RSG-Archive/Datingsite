<?php

$error_voornaam_empty = "You haven't entered your <u>First Name</u>!<br>";
$error_lastname_empty = "You haven't entered your <u>Last Name</u>!<br>";
$error_gender_empty = "You haven't entered your <u>Gender</u>!<br>";
$error_prefsex_empty = "You haven't entered your <u>Preferred Gender</u>!<br>";
$error_province_empty = "You haven't entered your <u>Province</u>!<br>";
$error_religion_empty = "You haven't entered your <u>Religion</u>!<br>";
$error_school_empty = "You haven't entered your <u>Education</u>!<br>";
$error_skin_empty = "You haven't entered your <u>Skin Color</u>!<br>";
$error_hair_empty = "You haven't entered your <u>Hair Color</u>!<br>";
$error_eye_empty = "You haven't entered your <u>Eye Color</u>!<br>";
$error_smoke_empty = "You haven't entered whether you <u>Smoke</u>!<br>";
$error_kids_empty = "You haven't entered your <u>Current amount of Children</u>!<br>";
$error_wish_empty = "You haven't entered your <u>Wanted amount of Children</u>!<br>";

$lang = array();
$lang['filling'] = "Filling in your Profile";
$lang['firstname'] = "What is your First Name?";
$lang['lastname'] = "What is your Last Name?";
$lang['gender'] = "What is your Gender?";
$lang['prefer'] = "Which gender do you Prefer?";
$lang['province'] = "In which Province (Netherlands) do you live?";
$lang['religion'] = "What is your Religion?";
$lang['school'] = "What is your Highest Achieved Education?";
$lang['skin'] = "What is your Skin Color?";
$lang['hair'] = "What is your Hair Color?";
$lang['eye'] = "What is your Eye Color?";
$lang['smoke'] = "Do you Smoke?";
$lang['kids'] = "How much children do you Currently have?";
$lang['wish'] = "How many Children do you still want?";
$lang['send'] = "Send";
?>