<?php

$profbericht = "Heeft nog geen bericht ingesteld.";
$titel = "Profiel van ";
$profileof = "Profiel van: ";
$persmsg = "Persoonlijk bericht: ";
$beroep = "Beroep: ";
$leeftijd = "Leeftijd: ";
$jaar = " jaar";
$email = "E-mail adres: ";

$info = "Informatie over mij: ";
$prefersex = "Ik val op het geslacht: "; 
$prov = "Ik woon in de provincie: ";
$haarkl = "Mijn haarkleur is: ";
$oogkl = "Mijn oogkleur is: ";
$huidskl = "Mijn huidskleur is: ";
$geloof = "Mijn geloof is: ";
$educ = "Mijn opleiding is: ";

$mailen = "Wilt u ons mailen? Dat kan! Klik ";
$mailen2 = "hier!";

$curkids = array();
$curkids[0] = "Ik heb nu ";
$curkids[1] = " kind";
$curkids[2] = "eren";

$kidwish = array();
$kidwish[0] = "Ik zou graag nog ";
$kidwish[1] = " kind";
$kidwish[2] = "eren";
$kidwish[3] = " willen krijgen";

$jarig = array();
$jarig[0] = "<b>--> Het is ";
$jarig[1] = "zijn";
$jarig[2] = "haar";
$jarig[3] = " verjaardag vandaag! Zeg 'Gefeliciteerd!' tegen ";
$jarig[4] = "hem";
$jarig[5] = "haar";
$jarig[6] = "!<b>";

?>