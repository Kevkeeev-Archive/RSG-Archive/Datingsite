<?php
$gender = array("Gender", "Male", "Female");
$prefer = array("Preference", "Male", "Female", "Both");
$province = array("Province", "Drenthe", "Flevoland", "Friesland", "Gelderland", "Groningen", "Limburg", "Noord-Brabant", "Noord-Holland", "Overijssel", "Utrecht", "Zeeland", "Zuid-Holland");
$religion = array("Religion", "Christian", "Catholic", "Reformed", "Protestant", "Moslim", "Boeddhism", "Hindoeism", "Jewish", "Other...", "Atheist" );
$school = array("Education", "VMBO", "HAVO", "VWO", "Grammar School", "Vocational", "College", "University");
$skin = array("Skin Colour", "White / Caucasian", "Lightly Tinted", "Medium Tinted", "Heavily Tinted", "Brown", "Black");
$hair = array("Hair Colour", "White", "Light Blonde", "Blonde", "Light Brown", "Brown", "Grey", "Black", "Light Red", "Red", "Other...");
$eye = array("Eye Colour", "Blue", "Blue Green", "Green", "Brown");
$smoke = array("Smoke", "I do smoke", "I do not smoke");
$kids = array("Children", "0", "1", "2", "3", "4", "5 or more");
$wish = array("Childwish", "0", "1", "2", "3", "4", "5 or more");

$maanden = array("January", "February", "March", "April", "May", "June", 
	"July", "August", "September", "October", "November", "December");

?>