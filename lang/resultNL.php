<?php
$lang = array();
$lang['match'] = "Matchen";
$lang['found1'] = "We hebben";
$lang['found2'] = "result";
$lang['found3'] = "aat";
$lang['found4'] = "aten";
$lang['found5'] = "gevonden:";
$lang['nomatch'] = "Wil je opnieuw matchen? <br><br>";
$lang['name'] = "Naam";
$lang['age'] = "Leeftijd";
$lang['gender'] = "Geslacht";
$lang['prefer'] = "Voorkeur";
$lang['province'] = "Provincie";
$lang['hair'] = "Haarkleur";
$lang['skin'] = "Huidskleur";
$lang['eye'] = "Oogkleur";
$lang['religion'] = "Geloof";
$lang['school'] = "Opleiding";
$lang['kids'] = "Kinderen";
$lang['wish'] = "Kinderwens";
$lang['smoke'] = "Roken";
$lang['sort'] = "Sorteer op:";
$lang['back'] = "Ga terug naar het match-scherm";
$lang['profile'] = "Ga naar mijn profiel";

$sort=array("Leeftijd", "Geslacht", "Voorkeur", "Provincie", "Haarkleur", 
	"Oogkleur", "Huidskleur", "Geloof", "Opleiding", "Kinderen", "Kinderwens", "Roken");
$wayer=array("Oplopend","Aflopend");

?>