<?php
$error_user_empty = "You haven't filled in your <u>Username</u>!<br />";
$error_user_taken = "That <u>Username</u> is already taken!<br />";
$error_pass_diff ="You haven't entered <u>the same</u> Password twice!<br />";
$error_pass_empty = "You need to enter your Password <u>twice</u>!<br />";
$error_pass_empty_both = "You haven't entered <u>either Password</u>!<br />";
$error_email_empty = "You haven't entered your <u>E-mail address</u>!<br />";
$error_leeftijd_empty = "You haven't entered your <u>Date of Birth</u>!<br />";
$error_leeftijd_tejong = "You are not <u>Old enough</u> to register!<br />";

$lang = array();
$lang["registration"] = "Registration";
$lang["register"] = "REGISTER";
$lang["username"] = "What is your Userrname?";
$lang["password"] = "What is your Password?";
$lang["controle"] = "Password for controle:";
$lang["email"] = "What is your E-mail address?";
$lang["geboorte"] = "What is your Date of Birth?";
$lang["leeftijd"] = "IMPORTANT! You have to be at least 18 years old!";
$lang["submit"] = "Submit";
$lang["reset"] = "Reset";

?>