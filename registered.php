<?php
session_start();
include_once('config.php');

if( !isset( $_GET['lang'])) { include_once('lang/registeredNL.php'); } else {
	switch( $_GET['lang'] ) {
		case "en" : include_once('lang/registeredEN.php'); break;
		case "nl" : 
		default : include_once('lang/registeredNL.php'); break;
	}
}
?>

<html>
<head>
<title><?php echo $lang['register']; ?> - <?php echo $sitename; ?></title>
<link rel="icon" type="image/ico" href="/favicon.ico"> </link>
</head>
<body bgcolor="#00BFFF">
<center><a href="<?php echo $homepage; ?>"><img border="0" src="profiles/logo.png" alt="<?php echo $sitename; ?> logo" width="435" height="264"></a></center> <br />
<b><?php echo $lang['database']; ?></b><br />
<table>
<tr><td width="105"><?php echo $lang['username']; ?>:</td> <td width="150"><?php echo $_SESSION['username']; ?></td></tr>
<tr><td><?php echo $lang['email']; ?>:</td> <td><?php echo $_SESSION['emailadres']; ?></td></tr>
<tr><td><?php echo $lang['birthday']; ?>:</td> <td><?php echo $_SESSION['dag'];?> - <?php echo $_SESSION['maand']; ?> - <?php echo $_SESSION['jaar']; ?></td></tr>
</table> <br />

<hr><br />
<b><?php echo $lang['login']; ?><b><br />
<table><form action="login.php" method="post">
<tr><td width="114"><style="font-weight;"><?php echo $lang['username']; ?>:</td>
<td width="200"><input type="text" size="30" name="username" value="<?php echo $_SESSION['username']; ?>"></td></tr>
<tr><td><style="font-weight;"><?php echo $lang['password']; ?>:</td>
<td><input type="password" size="30" name="password"> </td></tr>
<tr><td></td> <td><input type="submit" value="Log in!">
</form></td></tr></table>

<hr>
<form action="<?php echo $homepage; ?>" method="post" ><input type="submit" value='<?php echo $lang['homepage']; ?>'></form>
</body>
</html>
