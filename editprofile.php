<?php
include_once('config.php');

if(!isset($_SESSION)){ session_start(); }
IF (!isset($_SESSION['username']))
  	{ header('Location: '. $homepage); }
	
$con = openConnection();

$prof = mysql_fetch_assoc ( mysql_query ("SELECT Beroep, Profielbericht, Background FROM user WHERE Username = '".$_SESSION['username']."' ") );

IF( isset($_POST['controle']) AND $_POST['controle']==TRUE ) {

switch($_POST['bijwerken']) {

	case "bericht" : 
		IF ($_POST['bericht'] == "")
			{ $error_bericht = "Error! You haven't filled in your <u>Personal Message</u>!"; }
			ELSE {
				$sql_ber = ("
				UPDATE user
				SET Profielbericht = '".mysql_real_escape_string($_POST['bericht'])."'
				WHERE Username = '".$_SESSION['username']."'
				"); 
			IF (!mysql_query ($sql_ber, $con) )
				{ die ("Query Error: " . mysql_error()); }
			ELSE { header("Location: editprofile.php"); }
		}	
		break;
		
	case "achtergrond" : 
		IF ($_POST['achtergrond'] == "Achtergrondkleur" )
			{ $error_background = "Error! You haven't entered your <U>Background Color</u>!"; }
			ELSE { $sql_back = ("
				UPDATE user
				SET Background = '".$_POST['achtergrond']."'
				WHERE Username = '".$_SESSION['username']."'
				");
			IF (!mysql_query ($sql_back, $con) )
				{ die ("Query Error: " . mysql_error()); }
			ELSE { header("Location: editprofile.php"); }
		}
		break;
		
	case "beroep" : 
		IF ($_POST['beroep'] == "")
			{ $error_beroep = "Error! You haven't entered your <u>Profession</u>!"; }
			ELSE { $sql_prof = ("
				UPDATE user
				SET Beroep = '".mysql_real_escape_string($_POST['beroep'])."'
				WHERE Username = '".$_SESSION['username']."'
				");
			IF (!mysql_query ($sql_prof, $con) ) 
				{ die ("Query Error: " . mysql_error()); }
			ELSE { header("Location: editprofile.php");	}
		}
		break;
	
	case "password" : 
	
		IF ( ($_POST['pass1'] == "") OR ($_POST['pass2'] == "") OR ($_POST['pass3'] == "") ) 
			{ IF ($_POST['pass1'] == "") {$error_pass1="You haven't entered this field!"; }
			IF ($_POST['pass2'] == "") {$error_pass2="You haven't entered this field!"; }
			IF ($_POST['pass3'] == "") {$error_pass3="You haven't entered this field!"; }
			}
		ELSE IF ($_POST['pass1'] != $_POST['pass2']) {
			$error_pass1 = "You haven't entered the same password twice!";
			$error_pass2 = "You haven't entered the same password twice!";
		}
		ELSE {
			$sql_oldpw = mysql_fetch_assoc ( mysql_query ("SELECT Password, salt FROM user WHERE Username = '".$_SESSION['username']."' "), 0);
			IF ( md5(mysql_real_escape_string($_POST['pass3']) ).$sql_oldpw['salt'] != $sql_oldpw['Password'] )
				{ $error_pass3 = "This is not your current password!<br>Do you need help? Contact the admin <a href='mailto: ".$mailto." Password help'>here</a>!"; }
		
			ELSE { // hier gaat het eindelijk goed
				$sql_resetpw = (" UPDATE user 
				SET Password = '".md5($_POST['pass2'])."'
				WHERE Username = '".$_SESSION['username']."' ");
			IF (!mysql_query ($sql_resetpw, $con) )
				{ die ("Query Error: ". mysql_error()); }
			ELSE { header("Location: profile.php"); }
			}	
		}
		break;
} // end of switch

mysql_close($con);
}
?>

<html>
<head>
<title>Edit Profile</title>
<link rel="icon" type="image/ico" href="/favicon.ico"> </link>
</head>
<body bgcolor="#<?php echo $prof['Background']; ?>">

<?php include('menu.php'); ?>

<table> <!-- Beroep bewerken -->
<form action="" method="post">
<input type="hidden" name="bijwerken" value="beroep">
<input type="hidden" name="controle" value="TRUE">
<tr> <td width=250>What is your Profession?</td>
<td> <input type="text" name="beroep" size="30" value="<?php if(isset($prof['Beroep'])){echo $prof['Beroep'];} ?>"></td>
<td width=10></td> <td width=600><font color="#FF0000"> <?php if(isset($error_beroep)){echo $error_beroep;} ?> </font> </td></tr>
<tr><td></td> <td><input type="submit" value="Edit Profession"> </td></tr>
</form></table>
<hr>	

<table> <!-- Bericht toevoegen/bijwerken -->
<form action="" method="post">
<input type="hidden" name="bijwerken" value="bericht">
<input type="hidden" name="controle" value="TRUE">
<tr> <td width=250>What is your Personal Message?</td>
<td width=100>
<TEXTAREA name="bericht" rows="3" cols="40">
<?php echo $prof['Profielbericht']; ?></TEXTAREA></td>
<td width=10></td> <td width=600><font color="#FF0000"> <?php if(isset($error_bericht)){echo $error_bericht;} ?> </font> </td></tr>
<tr><td></td><td><input type="submit" value="Edit personal message"></td></tr>
</form> </table>
<hr>

<table style='text-align:center;vertical-align:middle'> <!-- Achtergrond bijwerken -->
<tr> <td width=250></td> <td bgcolor="#FFFFFF">WHITE</td>  <td bgcolor="#FF5EA4">PINK</td>	<td bgcolor="#BF27C4">PURPLE</td></tr>	
<tr> <td></td> <td bgcolor="#00FFFF">LIGHT BLUE</td>	<td bgcolor="#00BFFF">BLUE</td>	<td bgcolor="#0000FF">DARK BLUE</td></tr>
<tr> <td></td> <td bgcolor="#50FF50">LIGHT GREEN</td>	<td bgcolor="#00FF00">GREEN</td> 	<td bgcolor="#054A05">DARK GREEN</td> </tr>
<tr> <td></td> <td bgcolor="#FFFF00">YELLOW</td>	<td bgcolor="#FA830D">ORANGE</td> 	<td bgcolor="#FF0000">RED</td> </tr>
</table>

<table>
<form action="" method="post">
<input type="hidden" name="bijwerken" value="achtergrond">
<input type="hidden" name="controle" value="TRUE">
<tr><td width=250>Which Color do you want your Background to be?</td>
<td>
<?php
$colors=array("White", "Pink", "Purple", "Light Blue", "Blue", "Dark Blue", "Light Green", "Green", "Dark Green", "Yellow", "Orange", "Red");
$colorsHEX=array("FFFFFF", "FF5EA4", "BF27C4", "00FFFF", "00BFFF", "0000ff", "50FF50", "00FF00", "054A05", "FFFF00", "FA830D", "FF0000");

echo '<select name="achtergrond"> <option value="Achtergrondkleur">Background Color:</option>';
for ($c=0; $c<12; $c++)
	{if($colors[$c] == $prof['Background'] ){$selected = 'selected="selected"';}else{$selected = '';}
   echo '<option value="'.$colorsHEX[$c].'" '.$selected.'>'.$colors[$c].'</option>'; }
echo '</select>';
?> </td> 
<td width=10></td> <td><font color="#FF0000"> <?php if(isset($error_background)){echo $error_background;} ?> </font> </td></tr>
<tr><td></td><td><input type="submit" value="Edit background color"></td></tr>
</form> </table>

<hr>
<table> <!-- Achtergrondfoto uploaden -->
<form action="upload_file.php" method="post" enctype="multipart/form-data">
<tr><td width=250><label for="file">Profile Picture:</label></td><td><input type="file" name="file" id="file"><br /> </td></tr>
<tr> <td></td> <td><b>Important:</b> the picture may only be a '.jpg' or '.jpeg' or '.png' or '.gif' !!</td></tr>
<tr> <td></td> <td><b>Important:</b> the picture must be smaller than 50 kb !!</td></tr>
<tr><td></td> <td><input type="submit" name="submit" value="Upload Picture"></td></tr>
</form></table>

<hr>
<table> <!-- password veranderen -->
<form action="" method="post">
<input type="hidden" name="bijwerken" value="password">
<input type="hidden" name="controle" value="TRUE">
<tr><td width=250>Change your password:</td>
<td><input type="password" name="pass1" value="<?php if(isset($_POST['pass1'])){echo $_POST['pass1'];} ?>"></td> <td width=10></td>
<td width=600><font color="#FF0000"> <?php if(isset($error_pass1)){echo $error_pass1;} ?> </font> </td></tr>
<tr><td>Enter it again:</td>
<td><input type="password" name="pass2" value="<?php if(isset($_POST['pass2'])){echo $_POST['pass2'];} ?>"></td> <td width=10></td>
<td width=600><font color="#FF0000"> <?php if(isset($error_pass2)){echo $error_pass2;} ?> </font> </td></tr>
<tr><td>Enter your old password:</td>
<td><input type="password" name="pass3" value="<?php if(isset($_POST['pass3'])){echo $_POST['pass3'];} ?>"></td> <td width=10></td>
<td width=600><font color="#FF0000"> <?php if(isset($error_pass3)){echo $error_pass3;} ?> </font> </td></tr>
<tr><td></td> <td><input type="submit" value="Change Password"> </td> </tr>
</form></table>

<hr>
<form action="profile.php" method="post">
<input type="hidden" name="profiel" value="">
<input type="submit" value="Back to Profile"> </form>
</body>
</html>